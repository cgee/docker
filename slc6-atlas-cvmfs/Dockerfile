# SLC6 OS ATLAS nightly builds.

# Make the base image configurable:
ARG BASEIMAGE=cern/slc6-base:latest

# Set up the SLC6 "ATLAS OS":
FROM ${BASEIMAGE}

# Get user identity from host
ARG user_id
ARG group_id
ARG username
ARG groupname

# Perform the installation as root:
USER root
WORKDIR /root

# Put the repository configuration file(s) in place:
COPY *.repo /etc/yum.repos.d/
# Copy the prompt setup script in place:
COPY atlas_prompt.sh /etc/profile.d/

# Install extra SLC6 and LCG packages needed by the ATLAS release
RUN yum -y install which git wget tar atlas-devel texinfo ctags createrepo \
      redhat-lsb-core libX11-devel libXpm-devel libXft-devel libXext-devel \
      openssl-devel glibc-devel rpm-build nano sudo gcc-c++ HEP_OSlibs_SL6 \
      eos-client \
    && yum clean all

# Set up the user
RUN groupadd -g $group_id $groupname \
    && useradd -u $user_id -ms /bin/bash $username \
    && usermod -g $groupname $username

# Switch to the user account for the rest of the image
USER $username
WORKDIR /home/$username

# Start the image with BASH by default
CMD /bin/bash
